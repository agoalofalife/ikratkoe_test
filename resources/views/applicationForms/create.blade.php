@extends('layouts.layouts')
@section('content')

    <div class="row">
        <div class="col-md-6">
            <form role="form" method="post" action="{{url('/app')}}">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Имя</label>
                    <input type="text" class="form-control" name="name" id="exampleInputEmail1" placeholder="Имя">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Номер телефона</label>
                    <input type="text" class="form-control"  name="number" id="phone" placeholder="Номер телефона">
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Описание</label>
                    <textarea type="textarea" class="form-control" name="description" rows="10" id="exampleInputPassword1" placeholder="Добавить описание"></textarea>
                </div>
                <p>Регион</p>
                <select class="form-control" name="region">
                    @for($i = 0;$i<count($region);$i++)
                        <option>{{ $region[$i]->region}}</option>
                    @endfor
                </select>
                <br>
                <p>Статус</p>
                <select class="form-control" name="status">
                    @for($i = 0;$i<count($status);$i++)
                        <option>{{ $status[$i]->status}}</option>

                    @endfor
                </select>
                <br>
                <button type="submit" class="btn btn-default">Добавить</button>

            </form>
            <p id="messenger"></p>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>

    @endif

@stop
