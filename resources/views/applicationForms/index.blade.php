@extends('layouts.layouts')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-2">
            <form role="form" method="get" action="{{url('/app/filter')}}">
                {{csrf_field()}}
                <div class="form-group">
                    <p><strong>Выбрать регион</strong></p>
                    <select class="form-control" name="region">
                        <option selected></option>
                        @for($i=0;$i<count($region);$i++)
                             <option>{{$region[$i]->region}}</option>
                        @endfor
                    </select>
                    <p><strong>Выбрать статус</strong></p>
                    <select class="form-control" name="status">
                        <option selected></option>
                        @for($i=0;$i<count($status);$i++)
                            <option>{{$status[$i]->status}}</option>
                        @endfor
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Отправить</button>
            </form>
        </div>

        <div class="row">
            <div class="col-md-6">
                <table class="table">
                    <tr class="active"><th>Действие</th><th>Имя</th><th>Телефон</th><th>Описание</th><th>Регион</th><th>Статус</th></tr>
                    @foreach($list_application as $application)
                        <tr>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Изменить <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{url('/app/change?result=work&id='.$application->id)}}">Перевести в работу</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>{{$application->name}}</td>
                            <td>{{$application->phone_number}}</td>
                            <td>{{str_limit($application->description, $limit = 50, $end = '...')}}</td>
                            <td>{{$application->region}}</td>
                            <td>{{$application->status}}</td>
                            @if($application->status=='В работе')
                                <td><span class="glyphicon glyphicon-earphone"></span></td></tr>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
@stop