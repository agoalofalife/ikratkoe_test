<html>
<head>
    <title>Интерфейс обработки заявок</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

</head>
<body>

<ul class="nav nav-pills nav-stacked col-md-2" >
    <li class="active "><a href="{{url('/app')}}">Главная</a></li>
    <li><a href="{{url('/app/create')}}">Добавить заявку</a></li>
</ul>

@yield('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>