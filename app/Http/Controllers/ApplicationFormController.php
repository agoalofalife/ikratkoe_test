<?php

namespace App\Http\Controllers;
use App\ApplicationForm;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
class ApplicationFormController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['list_application'] = ApplicationForm::all();
        $data['region'] = DB::table('application_forms')->select('region')->distinct()->get();
        $data['status'] = DB::table('application_forms')->select('status')->distinct()->get();

        return view('applicationForms.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Находим регионы и статус для нашей формы
        $data['region'] = DB::table('application_forms')->select('region')->get();
        $data['status'] = DB::table('application_forms')->select('status')->distinct()->get();
        return view('applicationForms.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $this->validate($request, [
              'name'             => 'required|min:2',
              'description'      => 'required',
              'status'           =>'exists:application_forms,status',
              'region'           =>'exists:application_forms,region',
              'number'           =>'unique:application_forms,phone_number|integer'

          ],[
             'name.required'       => 'Поле Имя обязательно',
             'name.min'            =>'Поле Имя минимум 2 символа',
             'description.required'=>'Поле описание обязательно',
             'status.exists'       =>'Неверное значение поля СТАТУС',
             'region.exists'       =>'Неверное значение поля РЕГИОН',
             'number.integer'      =>'Поле телефон не является числом',
             'number.unique'       =>'Номер телефона уже зарегестрирован',

              ]);

          $add_form                    = new ApplicationForm;
          $add_form->name              = $request->name;
          $add_form->description       = $request->description;
          $add_form->phone_number      = $request->number;
          $add_form->region            = $request->region;
          $add_form->status            = $request->status;
          $result                      = $add_form->save();
          if($result){
              return redirect('/app')->with('success', 'Запись успешно добавлена');
          }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Функция возращает только отфильтрованные данные по региону и статусу
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter(Request $request)
    {
        if($request->region && $request->status){
            $data['list_application'] = ApplicationForm::where('region','=',$request->region)
                ->where('status','=',$request->status)
                ->get();
        }else{
            $data['list_application'] = ApplicationForm::Where('region','=',$request->region)
                ->orWhere('status','=',$request->status)
                ->get();
        }

    $data['status'] = DB::table('application_forms')
                      ->select('status')
                      ->distinct()->get();
    $data['region'] = DB::table('application_forms')
                      ->select('region')
                      ->distinct()
                      ->get();

        return view('applicationForms.index',$data);
    }

    /**
     * Функиция меняет статус у заявки
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change(Request $request)
    {
        $id_row     =  Input::get('id');
        switch(Input::get('result'))
        {
        case 'work':
               $insert_cell           = 'В работе';
               $update_status         =  ApplicationForm::find($id_row);
               $update_status->status =  $insert_cell;
               $update_status->save();

            return redirect('/app')->with('success', 'Статус изменени на : В работе');
        }
    }
}
