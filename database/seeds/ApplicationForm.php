<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class ApplicationForm extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        foreach (range(1,15) as $index) {
            DB::table('application_forms')->insert([
            'name'         => $faker->firstNameMale,
            'phone_number' => $faker->randomNumber,
            'region'       => $faker->region,
            'description'  => $faker->paragraph(),
            'status'       => $faker->randomElement($array = array('Подтверждена','Выполнена','Не выполнена','В работе')),
        ]);
    }
}
}
